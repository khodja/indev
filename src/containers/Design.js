import React, { Component }  from 'react'
import Design1 from '../images/design1.svg'
import Design2 from '../images/design2.svg'
import Design3 from '../images/design3.svg'
import { Link } from 'react-router-dom';

export default class Web extends Component {
    render() {
        return (
            <div className="home_bg">
                <div className="home_bg-wrapper animated">
                    <Link to={`/design/site`} className="home_bg-wrapper-site hover animated fadeInRight delay_3">
                        <img src={Design1} alt="photos" className="img" />
                        <h2 className="h2">Дизайн</h2>
                        <div className="p">Фирменный стиль</div>
                        <div className="p">Логотипы</div>
                        <div className="p">Брендбук</div>
                        <button className="inner-btn">Подробнее</button>
                    </Link>
                    <Link to={`/design/application`} className="home_bg-wrapper-site hover animated fadeInRight delay_4">
                        <img src={Design2} alt="photos" className="img" />
                        <h2 className="h2">Видео</h2>
                        <div className="p">Анимационный рекламный ролик</div>
                        <div className="p">Инфографическая презентация</div>
                        <div className="p">Имиджевый ролик</div>
                        <button className="inner-btn">Подробнее</button>
                    </Link>
                    <Link to={`/design/marketing`} className="home_bg-wrapper-site hover animated fadeInRight delay_5 hide">
                        <img src={Design3} alt="photos" className="img" />
                        <h2 className="h2">DEMO WORKS</h2>
                    </Link>
                </div>
            </div>            
        )
    }
}
