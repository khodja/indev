import React, { Component } from 'react'
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
    AccordionItemState
} from 'react-accessible-accordion';

export default class Mobile extends Component {
    render() {
        return (
            <div className="home_bg sm-column pr-20">
                <div className="mobile">
                    <div className="mobile-title animated fadeInRight slow delay_1">
                        Маркетинг
                    </div>
                    <div className="mobile-description animated fadeInRight slow delay_2">
                        SEO продвижение сайта в поисковых системах и SMM раскрутка в социальных сетях Вашей компании , продукции или услуги.
                    </div>
                    <Accordion allowZeroExpanded={true}>
                        <AccordionItem className="mobile-carousel animated fadeInRight slow delay_3">
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                <div className="top">
                                    <div className="title">SMM</div>
                                    <div className="price">от 2 000 000 сум</div>
                                </div>
                                <div className="bottom">
                                    <div className="descr">
                                        SMM-продвижение является одной из самых оптимальных видов рекламы, на данный момент. SMM-продвижение направленно на эмоциональное воздействие, учет предпочтений, настроения и интересов человека.
                                    </div>
                                    <div className="arrow">
                                    <AccordionItemState>
                                        {({expanded}) => (
                                            <span className={expanded ? 'open' : 'closed'}>
                                                <svg width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.4429 11.7687L1.08394 2.38387C0.777488 2.07638 0.778003 1.57856 1.08553 1.27159C1.39301 0.964858 1.89111 0.965651 2.1978 1.27317L10.9997 10.0994L19.8015 1.27286C20.1083 0.965374 20.606 0.964581 20.9136 1.27127C21.0677 1.42515 21.1448 1.62675 21.1448 1.82834C21.1448 2.02942 21.0682 2.23022 20.9152 2.38383L11.5565 11.7687C11.4091 11.9168 11.2086 11.9999 10.9997 11.9999C10.7908 11.9999 10.5905 11.9166 10.4429 11.7687Z" fill="#B58F69"/>
                                                </svg>
                                            </span>
                                        )}
                                    </AccordionItemState>
                                    </div>
                                </div>
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel >
                                <b className="gold">Стоимость</b> <br /> 
                                SMM-продвижение не требует таких крупных материальных вложений, как например баннерная или телевизионная реклама. Дополнительные затраты, для раскрутки бренда в соц сетях, требуются лишь для запуска таргетинговой рекламы.<br/>
                                <br />
                                <b className="gold">Скорость</b> <br /> 
                                Хорошо оформленная страница очень быстро может привлечь внимание. 10 человек поделятся страницей со своими друзьями, а те в свою очередь поделятся еще с 10 и так далее.<br/>
                                <br />
                                <b className="gold">Перспективность</b> <br />
                                 Активное развитие социальных сетей позволяет охватить широкую аудиторию потенциальных покупателей.<br/>
                                 <br />
                                <b className="gold">Конкурентоспособность</b> <br />
                                Пока, не все крупные компании, в достаточной мере, понимают важность развития рекламы и продвижение своего бренда. Это позволяет увеличить скорость и результативность от SMM раскрутки бизнеса.<br/>
                            </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem className="mobile-carousel animated fadeInRight slow delay_3">
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                <div className="top">
                                    <div className="title">SEO</div>
                                    <div className="price">от 2 000 000 сум</div>
                                </div>
                                <div className="bottom">
                                    <div className="descr">
                                        Услуги поисковой оптимизации и продвижения сайта в поисковых системах. SEO-продвижение сайта — это "сердце" интернет-маркетинга.
                                    </div>
                                    <div className="arrow">
                                    <AccordionItemState>
                                        {({expanded}) => (
                                            <span className={expanded ? 'open' : 'closed'}>
                                                <svg width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.4429 11.7687L1.08394 2.38387C0.777488 2.07638 0.778003 1.57856 1.08553 1.27159C1.39301 0.964858 1.89111 0.965651 2.1978 1.27317L10.9997 10.0994L19.8015 1.27286C20.1083 0.965374 20.606 0.964581 20.9136 1.27127C21.0677 1.42515 21.1448 1.62675 21.1448 1.82834C21.1448 2.02942 21.0682 2.23022 20.9152 2.38383L11.5565 11.7687C11.4091 11.9168 11.2086 11.9999 10.9997 11.9999C10.7908 11.9999 10.5905 11.9166 10.4429 11.7687Z" fill="#B58F69"/>
                                                </svg>                                                
                                            </span>
                                        )}
                                    </AccordionItemState>
                                    </div>
                                </div>
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel >
                                • Вывод в топ 10 или 3 по определенным ключевым словам <br/>
                                • Привлечение большого количества аудитории и потенциальных покупателей <br/>
                                • Улучшение имиджа (авторитета) компании <br/>
                                • Получение максимальной отдачи при минимальных вложениях <br/>
                            </AccordionItemPanel>
                        </AccordionItem>
                    </Accordion>
                </div>
            </div>
        )
    }
}
